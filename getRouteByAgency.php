<?php

require_once "include.lib.php";
$agency = $_GET['a'];
$list = isset($_GET['list']);
//get the list of routes
$information = pull_url("http://proximobus.appspot.com/agencies/$agency/routes.json");

//start the page
echo "<ANGELXML>
<QUESTION var=\"Route\">
<PLAY>
";

if ($list) {
    play_prompt("21001");
    if ($agency == 'mbta') {
        echo "<PROMPT type=\"text\">\n";
        foreach ($information->items as $item) {
            if (strpos($item->display_name, "/") !== false)
                continue;   //skip routes with a slash
 echo $item->display_name . ",\n";
        }
        echo "</PROMPT>";
    } else {
        $rts = array("daytime", "tech", "beast", "bwest", "ceast", "cwest");
        foreach ($rts as $r)
            play_prompt($r);
    }
    play_prompt("again-repeat");
} else {
    play_prompt("20501-$agency");
}

echo "</PLAY>
<RESPONSE confirm=\"false\">
<KEYWORD>\n";

foreach ($information->items as $item) {
    if (strpos($item->display_name, "/") !== false)
        continue;   //skip routes with a slash
 if ($item->id == 'saferidebostonall' || $item->id == 'saferidecamall' ||
            $item->id == 'northwest')
        continue;
    $keywords[] = $item->display_name;
    if ($item->display_name != $item->id)
        $keywords[] = $item->id;
    if ($agency == "mbta"):
        $keywords[] = "Number " . $item->id;
        $keywords[] = "Route " . $item->id;
        $keywords[] = "Number " . $item->id . " bus";
    endif;
    if (strpos($item->id, "saferide") !== false)
        $keywords[] = ltrim(str_replace("Saferide", "", $item->display_name));
    if ($item->id == 'boston')
        $keywords[] = "Daytime";
    echo "<LINK keyword=\"" . implode(",", $keywords) . "\" returnValue=\"{$item->id}\"" .
    " destination=\"" . SITE . "/3000\" />\n";
    unset($keywords);
}
if (!$list)
    echo "<LINK keyword=\"list,list them\" returnValue=\"l\" destination=\"" . SITE . "/2100\" /> \n";
else
    echo "<LINK keyword=\"repeat,start over,say them again,say again\" returnValue=\"l\" destination=\"/2100\" /> \n";
echo "<LINK keyword=\"go back,previous,back\" returnValue=\"r\" destination=\"/2000\" />
</KEYWORD>
</RESPONSE>\n";

echo "<ERROR_STRATEGY type=\"noinput\" reprompt=\"false\">\n";
if ($list)
    play_prompt("21002");
else
    play_prompt("20502");
echo "<PROMPT type=\"text\"></PROMPT><GOTO destination=\"" . SITE . "/7000\" />\n";
echo "</ERROR_STRATEGY>\n";

echo "<ERROR_STRATEGY type=\"nomatch\" reprompt=\"false\">\n";
$p = ($list) ? "21003" : "20503";
$p .= ( $agency == "mbta") ? "-number" : "-name";
play_prompt($p);

echo "<PROMPT type=\"text\"></PROMPT><GOTO destination=\"" . SITE . "/7000\" />\n";
echo "</ERROR_STRATEGY>\n";

echo "</QUESTION>\n</ANGELXML>";
ob_end_flush();