<ANGELXML>
    <QUESTION var="Stop">
        <PLAY>
            <?php play_prompt('30001-beacon') ?>
        </PLAY>
        <RESPONSE>
            <KEYWORD>
                <LINK keyword="to cambridge,to campus,back to campus,right before campus,before campus" dtmf="1"
                      returnValue="13" destination="/5000" />
                <LINK keyword="to boston,after the bridge,from campus,right after the bridge" dtmf="2"
                      returnValue="29" destination="/5000" />
                <LINK keyword="go back,back,previous" returnValue="l" destination="/4000" />
            </KEYWORD>
        </RESPONSE>
        <ERROR_STRATEGY type="noinput" reprompt="false">
            <?php
            play_prompt("30002-beacon");
            ?>
            <PROMPT type="text">
            </PROMPT>
        </ERROR_STRATEGY>
        <ERROR_STRATEGY type="nomatch" reprompt="false">
            <?php
            play_prompt("30003-beacon");
            ?>
            <PROMPT type="text">
            </PROMPT>
        </ERROR_STRATEGY>
        <GOTO destination="/7000" />
    </QUESTION>
</ANGELXML>
<?php
            exit;