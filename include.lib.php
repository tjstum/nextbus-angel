<?php

define("SITE", "200000114374");
ob_start();

function pull_url($url) {
    $response = file_get_contents($url);
    return json_decode($response);
}

function name_route($id, $agency) {
    $n = pull_url("http://proximobus.appspot.com/agencies/$agency/routes/$id.json")->display_name;
    if ($agency == 'mit')
        return $n;
    return "Route $n";
}

function name_run($run, $route, $agency) {
    $n = pull_url("http://proximobus.appspot.com/agencies/$agency/routes/$route/runs/$run.json")->display_name;
    return $n;
}

function name_stop($stop, $agency) {
    return pull_url("http://proximobus.appspot.com/agencies/$agency/stops/$stop.json")->display_name;
}

function str_prompt($promptName) {
    return "http://stum.scripts.mit.edu/nextbus/audio/small/$promptName.wav";
}

function play_prompt($promptName, $suppress = false) {
    $e = "<PROMPT type=\"externalaudio\">\n" . str_prompt($promptName) . "\n</PROMPT>\n";
    if (!$suppress)
        echo $e;
    return $e;
}

function play_route_id($route, $ok = false, $agency = 'mit', $suppress = false) {
    $map['boston'] = 'daytime';
    $map['tech'] = 'tech';
    $map['saferidebostone'] = 'beast';
    $map['saferidebostonw'] = 'bwest';
    $map['saferidecambeast'] = 'ceast';
    $map['saferidecambwest'] = 'cwest';
    if (!array_key_exists($route, $map))
        return play_text_route($route, $agency, $suppress);
    $f = $map[$route];
    if ($ok)
        $f = "ok-" . $f;
    return play_prompt($f, $suppress);
}

function play_text_route($route, $agency, $suppress = false) {
    return play_text(name_route($route, $agency), $suppress);
}

function play_text($text, $suppress = false) {
    $e = "<PROMPT type=\"text\">\n$text\n</PROMPT>";
    if (!$suppress)
        echo $e;
    return $e;
}

function get_stop_nicknames($stop) {
    $e['03'] = array("Student Center", "The Student Center", "MIT");
    $e['02'] = array("Seventy Seven");
    $e['13'] = array("Beacon", "Beacon Street");
    $e['21'] = array("Four Seventy Eight Comm Ave", "Alpha Chi Omega", "A Chi Oh", "Ess Kay", "Sigma Kappa");
    $e['22'] = array("Four Eighty Seven Comm Ave", "A fee", "Phi Sig", "Phi Sigma Kappa", "Alpha Phi");
    $e['58'] = array("MBTA stop", "Newbury Street at Mass Ave", "Hynes Station", "Hynes Convention Center");
    $e['26'] = array("32 Hereford", "Chi Phi");
    $e['12'] = array("Sigma Chi", "Sig Ep", "Skullhouse", "Theta Chi");
    $e['49'] = array("Epsilon Theta", "Two Fifty Nine Saint Paul Street");
    $e['08'] = array("Student House", "Beta", "One Eleven Bay State", "One Eleven Bay State Road");
    $e['09'] = array("A E Phi", "Alpha Epsilon Phi", "One Fifty Five Bay State");
    $e['28'] = array('Zee Bee Tee', "Z B T", "Zeta Beta Tau", "Fifty Eight Manchester Street");
    return (array_key_exists($stop, $e)) ? $e[$stop] : array();
}

function play_stop($stop, $agency, $suppress = false) {
    $e['03'] = '84-mass';
    $e['02'] = '77-mass-short-2';
    $e['13'] = 'mass-beacon';
    $e['29'] = 'mass-beacon';
    $e['21'] = '478-comm';
    $e['22'] = '487-comm';
    $e['58'] = 'mass-newbury';
    $e['26'] = '32-hereford';
    $e['12'] = '528-beacon';
    $e['49'] = '259-stpaul';
    $e['08'] = '111-bayst';
    $e['09'] = '155-baayst';
    $e['28'] = '58-manch';
    $e['24'] = 'east-main';
    $e['39'] = '70-pacific';
    $e['38'] = 'random';
    $e['04'] = '6th-charles';
    $e['17'] = 'camb-5th';
    $e['05'] = 'ames-66';
    $e['33'] = 'medical-carleton';
    $e['18'] = '638-camb';
    $e['01'] = 'kendall';
    $e['37'] = 'warehouse';
    $e['36'] = 'edgerton';
    $e['53'] = 'amherst-wadsworth';
    $e['41'] = 'portland-hampshire';
    $e['27'] = 'main-windsor';
    $e['23'] = 'edgerton';
    $e['45'] = 'river-franklin';
    $e['14'] = 'brookline-chestnut';
    $e['16'] = 'burton';
    $e['55'] = 'ww15';
    $e['47'] = 'simmons';
    $e['44'] = 'river-fairmont';
    $e['30'] = 'mccormick';
    $e['51'] = 'tang-westgate';
    $e['43'] = 'putnam-magazine';
    $e['35'] = 'new-house';
    $e['50'] = 'sydney-green';
    $e['46'] = 'river-pleasant';
    $e['40'] = '70-pacific';
    $e['54'] = 'warehouse';
    $e['53'] = 'amherst-wadsworth';
    $e['07'] = 'amherst-wadsworth';
    $e['48'] = 'stata';
    $e['57'] = 'w92-amesbury';
    $e['52'] = 'vassar-mass';
    $e['60'] = 'media-lab';
    $e['61'] = 'kresge';

    if ($agency == 'mit' && array_key_exists($stop, $e))
        return(play_prompt($e[$stop], $suppress));
    return play_text(name_stop($stop, $agency), $suppress);
}