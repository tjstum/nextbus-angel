<?php

/* this page handles the getting of stops when it's an MIT route, since MIT routes
  don't duplicate stops very much. the only one that really ends up mattering is beast/daytime,
  and there's sometimes a distinction between 77 mass ave and 84 mass ave
 */


//page start
echo "<ANGELXML>
<QUESTION var=\"Stop\">
<PLAY>";
if (!$list) {
//first we need to confirm the route
    play_route_id($route, true);
//now play prompts
    play_prompt("40001");
    if ($route == 'saferidebostone' || $route == 'boston')
        play_prompt("things-like");
    play_prompt("just-mit");
} else {
    play_prompt("41001");
    $stops['boston'] = array('84-mass-2', 'mass-beacon', '487-comm', 'comm-sherborn', '478-comm', '77-mass-short-2', 'edgerton');
    $stops['tech'] = array('kendall', 'amherst-wadsworth', 'media-lab', 'kresge', 'burton', 'tang-westgate', 'w92-amesbury',
        'simmons', 'vassar-mass', 'stata');
    $stops['saferidebostone'] = array('mass-newbury', '84-mass', 'mass-beacon', '478-comm', '487-comm', '77-mass-short-2');
    $stops['saferidebostonw'] = array('84-mass', '528-beacon', '487-comm', '111-bayst', '155-bayst', '259-stpaul',
        '58-manch', '32-hereford', '77-mass-short-2');
    $stops['saferidecambe'] = array('84-mass', 'edgerton', 'warehouse', '70-pacific', 'random', 'main-windsor', 'portland-hampshire',
        '638-camb', 'camb-5th');
    $stops['saferidecambw'] = array('84-mass', 'mccormick', 'burton', 'new-house', 'tang-westgate', 'simmons', 'ww15',
        'brookline-chestnut', 'putnam-magazine', 'river-fairmont', 'river-pleasant', 'river-franklin', 'sydney-green',
        '70-pacific', 'warehouse', 'edgerton', 'kendall', 'amherst-wadsworth', '77-mass-short-2');
    foreach ($stops[$route] as $s) {
        play_prompt($s);
    }
    play_prompt("again-repeat");
}
echo "</PLAY>
<RESPONSE>
<KEYWORD>\n";

$information = pull_url("http://proximobus.appspot.com/agencies/mit/routes/$route/stops.json");

foreach ($information->items as $item) {
    $keywords = array();
    if ($item->id == '29')
        continue; //we have to do this separately, later
 $dn = str_replace("@", "at", $item->display_name);
    $dn = str_replace("-", "", $dn);
    if (strpos($dn, "/") !== false) {
        $names = explode("/", $dn);
        $keywords = array_merge($keywords, $names);
    } else {
        $keywords[] = $dn;
    }
    $keywords = array_merge($keywords, get_stop_nicknames($item->id));
    $dest = ($item->id == '13') ? '3000' : '5000';
    echo "<LINK keyword=\"" . implode(",", $keywords) . "\" returnValue=\"{$item->id}\"" .
    " destination=\"" . SITE . "/$dest\" />\n";
}
if (!$list)
    echo "<LINK keyword=\"list,list them\" returnValue=\"l\" destination=\"" . SITE . "/4100\" /> \n";
else
    echo "<LINK keyword=\"repeat,start over,say them again,say again\" returnValue=\"l\" destination=\"/4100\" /> \n";
echo "<LINK keyword=\"back up,go back,previous,back\" returnValue=\"b\" destination=\"/2050\" />
</KEYWORD>
</RESPONSE>\n";

echo "<ERROR_STRATEGY type=\"noinput\" reprompt=\"false\">\n";
if ($list)
    play_prompt("41002");
else
    play_prompt("40002");
echo "<PROMPT type=\"text\"></PROMPT><GOTO destination=\"" . SITE . "/7000\" />\n";
echo "</ERROR_STRATEGY>\n";

echo "<ERROR_STRATEGY type=\"nomatch\" reprompt=\"false\">\n";
if ($list)
    play_prompt("41003");
else
    play_prompt("40003");
echo "<PROMPT type=\"text\"></PROMPT><GOTO destination=\"" . SITE . "/7000\" />\n";
echo "</ERROR_STRATEGY>\n";

echo "</QUESTION>
<VARIABLES>
<VAR name=\"ForceMit\" value=\"true\" />
</VARIABLES>
</ANGELXML>";

exit;