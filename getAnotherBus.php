<?php
require_once "include.lib.php";
?>
<ANGELXML>
    <QUESTION>
        <PLAY>
            <?php play_prompt('60001'); ?>
        </PLAY>
        <RESPONSE>
            <YES_NO>
                <YES destination="/2000" />
                <NO destination="/6001" />
            </YES_NO>
        </RESPONSE>
        <ERROR_STRATEGY type="noinput" reprompt="false">
            <?php play_prompt('60002'); ?>
            <PROMPT type="text">
            </PROMPT>
        </ERROR_STRATEGY>
        <ERROR_STRATEGY type="nomatch" reprompt="false">
            <?php play_prompt('60003'); ?>
            <PROMPT type="text">
            </PROMPT>
        </ERROR_STRATEGY>
        <GOTO destination="/7000" />
    </QUESTION>
</ANGELXML>
