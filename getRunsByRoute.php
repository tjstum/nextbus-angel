<?php

require_once "include.lib.php";
$agency = $_GET['a'];
$route = $_GET['rt'];
$forceMit = ($_GET["f"] == "true");

//first, let's check the first stop, and if it's not running, let's send the error
$information = pull_url("http://proximobus.appspot.com/agencies/$agency/routes/$route/stops.json");
$i = 0;
do {
    $id = $information->items[$i]->id;
    $i++;
} while (!$id);
$check = pull_url("http://proximobus.appspot.com/agencies/$agency/stops/$id/predictions/by-route/$route.json");
if (count($check->items) == 0) {
    echo "<ANGELXML bargein=\"false\">
<MESSAGE>
<PLAY>
";
    play_prompt("30002");
    echo "</PLAY>
<GOTO destination=\"" . SITE . "/2050\" />
</MESSAGE></ANGELXML>";
    ob_end_flush();
    exit;
}
//if this is MIT, we're going to be edgy and skip this page. we'll set the run
//to mit just so that Angel doesn't freak out later
if ($agency == 'mit' && !$forceMit) {
    echo "<ANGELXML>
<MESSAGE>
<PLAY>
</PLAY>
<GOTO destination=\"" . SITE . "/4000\" />
</MESSAGE>
<VARIABLES>
<VAR name=\"Run\" value=\"mit\" />
</VARIABLES>
</ANGELXML>";
    ob_end_flush();
    exit;
}

if ($agency == 'mit')
    require 'disambiguateBeacon.inc.php';

//get the list of runs for this route
$information = pull_url("http://proximobus.appspot.com/agencies/$agency/routes/$route/runs.json");
echo "<ANGELXML>
<QUESTION var=\"Run\">
<PLAY>";

//ok, if this is a TTS route, play the message and set the flag
$tts = false;
if ($route != '1' && $route != '701'):
    $tts = true;
    play_prompt("not-well");
endif;

if (!$tts) {
    $prompt = "30001-";
    $prompt .= ( $route == '701') ? "ct1" : "1";
    play_prompt($prompt);
} else {
    echo "<PROMPT type=\"text\">
OK, which direction for " . name_route($route, $agency) . "? You can say ";
    $or = false;
    foreach ($information->items as $run) {
        echo ($or) ? " or " : "";
        echo $run->display_name;
        $or = true;
    }
    echo "\n</PROMPT>";
} echo "\n</PLAY>

<RESPONSE confirm = \"false\">
<KEYWORD>\n";

$i = 1;
foreach ($information->items as $run) {
    $dName = str_replace("-", " ", $run->display_name);
    $keywords[] = str_replace(".", "", $dName);
    if (($run->display_name != $run->id) && $agency != 'mbta')
        $keywords[] = $run->id;
    if ($run->direction_name)
        $keywords[] = $run->direction_name;
    if (!$tts)  //the ones that run in front of MIT
        $keywords[] = ($run->direction_name == 'Outbound') ? "to cambridge,cambridge" : "to boston,boston";
    echo "<LINK keyword=\"" . implode(",", $keywords) . "\" returnValue=\"{$run->id}\"" .
    " destination=\"" . SITE . "/4000\" dtmf=\"$i\" /> \n";
    unset($keywords);
    $i++;
}
echo "<LINK keyword=\"go back,previous,back\" returnValue=\"r\" destination=\"/2050\" />
</KEYWORD>
</RESPONSE>\n";
echo "<ERROR_STRATEGY type=\"noinput\" reprompt=\"false\">";
if ($tts) {
    play_prompt("30003-init");
    play_text($information->items[0]->display_name);
    play_prompt("30003-trans");
    play_text($information->items[1]->display_name);
    play_prompt("30003-final");
} else {
    play_prompt("30003-new");
}
echo "<PROMPT type=\"text\"></PROMPT><GOTO destination=\"" . SITE . "/7000\" />\n";
echo "</ERROR_STRATEGY>\n";

echo "<ERROR_STRATEGY type=\"nomatch\" reprompt=\"false\">";
if ($tts) {
    play_prompt("30004-init");
    play_text($information->items[0]->display_name);
    play_prompt("30004-trans");
    play_text($information->items[1]->display_name);
    play_prompt("30004-final");
} else {
    play_prompt('30004-new');
}
echo "<PROMPT type=\"text\"></PROMPT><GOTO destination=\"" . SITE . "/7000\" />\n";
echo "</ERROR_STRATEGY>\n";

echo "</QUESTION>\n</ANGELXML>";
ob_end_flush();