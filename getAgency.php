<?php
require_once "include.lib.php";
$checkpoint = $_GET["checkpoint"] == "true";
?>
<ANGELXML bargein="true">
    <QUESTION var="Agency">
        <PLAY>
            <?php
            if ($checkpoint)
                play_prompt("10001");
            play_prompt("20001");
            ?>
        </PLAY>
        <RESPONSE>
            <KEYWORD>
                <LINK dtmf="1" returnValue="mbta" keyword="m b t a" destination="/2050" />
                <LINK dtmf="2" returnValue="mit" keyword="m i t" destination="/2050" />
            </KEYWORD>
        </RESPONSE>
        <ERROR_STRATEGY type="noinput" reprompt="false">
            <?php
            play_prompt("20002");
            play_prompt("20003");
            ?>
            <PROMPT type="text">
            </PROMPT>
        </ERROR_STRATEGY>
        <ERROR_STRATEGY type="nomatch" reprompt="false">
            <?php
            play_prompt("20004");
            play_prompt("20005");
            ?>
            <PROMPT type="text">
            </PROMPT>
        </ERROR_STRATEGY>
        <GOTO destination="/7000" />
    </QUESTION>
    <VARIABLES>
        <VAR name="Checkpoint" vaue="false" />
        <VAR name="ForceMit" value="false" />
    </VARIABLES>
</ANGELXML>