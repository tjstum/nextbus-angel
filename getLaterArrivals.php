<?php
require_once "include.lib.php";
?>
<ANGELXML>
    <QUESTION>
        <PLAY>
            <?php
            play_prompt("50501");
            ?>
        </PLAY>
        <RESPONSE>
            <YES_NO>
                <YES destination="/5100" />
                <NO destination="/6000" />
            </YES_NO>
        </RESPONSE>
        <ERROR_STRATEGY type="noinput" reprompt="false">
            <?php
            play_prompt("50502");
            ?>
            <PROMPT type="text">
            </PROMPT>
        </ERROR_STRATEGY>
        <ERROR_STRATEGY type="nomatch" reprompt="false">
            <?php
            play_prompt("50503");
            ?>
            <PROMPT type="text">
            </PROMPT>
        </ERROR_STRATEGY>
        <GOTO destination="/7000" />
    </QUESTION>
</ANGELXML>
