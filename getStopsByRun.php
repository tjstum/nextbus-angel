<?php

require_once "include.lib.php";
$agency = $_GET['a'];
$route = $_GET['rt'];
$run = $_GET['rn'];
$list = isset($_GET["list"]);

if ($agency == 'mit')
    require 'unifiedGetStops.inc.php';

$information = pull_url("http://proximobus.appspot.com/agencies/$agency/routes/$route/runs/$run/stops.json");
$route_info = pull_url("http://proximobus.appspot.com/agencies/$agency/routes/$route.json");
$run_info = pull_url("http://proximobus.appspot.com/agencies/$agency/routes/$route/runs/$run.json");

//if there is only one stop, go directly there.

if (count($information->items) == 1) {
    echo "<ANGELXML>
<MESSAGE>
<PLAY>
</PLAY>
<GOTO destination=\"" . SITE . "/5000\" />
</MESSAGE>
<VARIABLES>
<VAR name=\"Stop\" value=\"{$information->items[0]->id}\" />
</VARIABLES>
</ANGELXML>";
    ob_end_flush();
    exit;
}

//start
echo "<ANGELXML>
<QUESTION var=\"Stop\">
<PLAY>
\n";

if ($list) {
    play_prompt('41001');
    $text = "";
    foreach ($information->items as $item) {
        $text .= $item->display_name . ",\n";
    }
    play_text($text);
    play_prompt("again-repeat");
} else {
    if ($agency == '1' || $agency == '701') {
        $prompt = 'ok-';
        if ($agency == '1'):
            $prompt.="1-";
            $prompt.= ( $run == '1_10017v0_0') ? 'cam' : 'bos';
        else:
            $prompt.="ct1-";
            $prompt.= ( $run == '701_7010021v0_0') ? 'cam' : 'bos';
        endif;
        play_prompt($prompt);
        play_prompt('just-mit');
    } else {
        play_text(name_route($route, $agency) . " - " . name_run($run, $route, $agency));
    }
    play_prompt('40001');
}

echo "
</PLAY>
<RESPONSE confirm=\"false\">
<KEYWORD>\n";
$allwords = array();
foreach ($information->items as $item) {
    if (!$item->id)
        continue;
    $dn = str_replace("@", "at", $item->display_name);
    $dn = str_replace("-", "", $dn);
    $dn = str_replace("/", "", $dn);
    if ($item->id == '00075' || $item->id == '00097'):
        $keywords[] = 'M I T';
        $keywords[] = 'Student Center';
    endif;
    if (array_search($dn, $allwords))
        continue;
    $allwords[] = $dn;
    $keywords[] = $dn;
    echo "<LINK keyword=\"" . implode(",", $keywords) . "\" returnValue=\"{$item->id}\"" .
    " destination=\"" . SITE . "/5000\" />\n";
    unset($keywords);
}
if (!$list)
    echo "<LINK keyword=\"list,list them\" returnValue=\"l\" destination=\"" . SITE . "/4100\" /> \n";
else
    echo "<LINK keyword=\"repeat,start over,say them again,say again\" returnValue=\"l\" destination=\"/4100\" /> \n";
echo "<LINK keyword=\"go back,previous,back\" returnValue=\"r\" destination=\"/3000\" />
</KEYWORD>
</RESPONSE>\n";

echo "<ERROR_STRATEGY type=\"noinput\" reprompt=\"false\">\n";
if ($list)
    play_prompt("41002");
else
    play_prompt("40002");
echo "<PROMPT type=\"text\"></PROMPT><GOTO destination=\"" . SITE . "/7000\" />\n";
echo "</ERROR_STRATEGY>\n";

echo "<ERROR_STRATEGY type=\"nomatch\" reprompt=\"false\">\n";
if ($list)
    play_prompt("41003");
else
    play_prompt("40003");
echo "<PROMPT type=\"text\"></PROMPT><GOTO destination=\"" . SITE . "/7000\" />\n";
echo "</ERROR_STRATEGY>\n";

echo "</QUESTION>\n</ANGELXML>";
ob_end_flush();