<?php

require_once "include.lib.php";

$agency = $_GET["a"];
$stop = $_GET["s"];
$route = $_GET["rt"];
$run = $_GET["rn"];
$later = isset($_GET["later"]) ? 2 : 0;

//ask the API
$information = pull_url("http://proximobus.appspot.com/agencies/$agency/stops/$stop/predictions/by-route/$route.json");

//list the common info
echo "<ANGELXML bargein=\"false\">
<MESSAGE>
<PLAY>
";

//get arrivals ready
foreach ($information->items as $item) {
    if (($item->seconds < 60)) {
        continue;
    }
    $arrival[] = $item->minutes;
}

if (!isset($_GET['later'])) {
    play_prompt("50001-init");
    play_text($arrival[0]);
    play_prompt("50001-minutesOn");
    play_route_id($route, false, $agency);
    play_prompt("at");
    play_stop($stop, $agency);
    play_prompt("50001-arrive");
} else {
    play_prompt("51001-intro");
    play_text($arrival[1]);
    if ($arrival[2]):
        play_prompt('51001-and');
        play_text($arrival[2]);
    endif;
    play_prompt('51001-afterThe');
    play_text($arrival[0]);
    play_prompt('51001-minutes');
}

//common transition
echo "</PLAY>";
echo "<GOTO destination=\"" . SITE;
echo (isset($_GET['later']) || !$arrival[1]) ? "/6000" : "/5050";
echo "\" />";
echo "</MESSAGE>";

echo "<VARIABLES>
<VAR name=\"ForceMit\" value=\"false\" />
</VARIABLES>";

echo "</ANGELXML>";
ob_end_flush();